
import 'package:flutter/material.dart';

class AwesomePainter extends CustomPainter {
Paint _paint;

AwesomePainter(){
  _paint = Paint()
  ..color = Colors.yellow
  ..strokeWidth = 10.0
  ..style = PaintingStyle.fill;

}

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(size.width/2, size.height/2), size.width/4, _paint);
  }
}
