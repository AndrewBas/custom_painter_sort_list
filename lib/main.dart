import 'package:flutter/material.dart';

import 'awesome_painter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<int> someList = List<int>.generate(100, (int index) => index);

  void _sort() {
    setState(() {
      print('_sort');
      if (someList[0] < someList[1]) {
        someList.sort((a, b) => b.compareTo(a));
      } else {
        someList.sort((a, b) => a.compareTo(b));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // double quarterScreen = MediaQuery.of(context).size.height * 0.25;
    return Scaffold(

      body: Center(
        child: CustomPaint(
          size:  MediaQuery.of(context).size,
          painter: AwesomePainter(),
          child: ListView.builder(
            itemBuilder: (ctx, id) {
              return Container(
                  height: 40,
                  child: Text(
                    '${someList[id]}',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.center,
                  ));
            },
            itemCount: someList.length,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _sort,
        tooltip: 'Increment',
        child: Icon(Icons.sort),
      ),
    );
  }
}
